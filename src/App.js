
import './App.css';
import NavigationBar from './components/Navbar';
import ItemCard from './components/ItemCard'
import { getAllProducts } from './service/api.js';
import {useEffect, useState} from 'react';


function App() {

  let [products,setProducts] = useState([]);

  let fetchProducts = async () =>{
    let data = await getAllProducts();
    console.log(data)
    setProducts(data);
  }

  useEffect(()=>{
    fetchProducts();
  },[])

  return (
    <>
    <NavigationBar />
   
     <main className="grid-container">
      {products.length>0&&(
        products.map(item => {
          return <ItemCard item={item} key={item.name}/>
        })
      )
      }

     </main>



    
  
    </>
    
  );
}

export default App;
